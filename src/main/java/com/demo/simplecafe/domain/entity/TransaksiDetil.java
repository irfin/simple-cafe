/*
 * TransaksiDetil.java
 *
 * Created on May 07, 2021, 3:10 PM
 */
package com.demo.simplecafe.domain.entity;

import lombok.Data;

import javax.persistence.*;

/**
 * @author Irfin A
 */
@Data
@Entity
@Table(name="transaksi_detil")
public class TransaksiDetil
{
    @Id
    private Long id;

    @ManyToOne
    private Transaksi tx;

    @ManyToOne
    private Minuman minuman;

    @Column
    private int qty;

    @Column
    private double harga;
}
