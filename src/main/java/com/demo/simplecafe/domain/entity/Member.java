/*
 * Member.java
 *
 * Created on May 07, 2021, 3:03 PM
 */
package com.demo.simplecafe.domain.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Irfin A
 */
@Data
@Entity
@Table(name = "member")
public class Member
{
    @Id
    private Long id;

    @Column
    private String nama;

    @Column
    private String level;
}
