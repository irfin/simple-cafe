/*
 * Transaksi.java
 *
 * Created on May 07, 2021, 3:06 PM
 */
package com.demo.simplecafe.domain.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * @author Irfin A
 */
@Data
@Entity
@Table(name="transaksi")
public class Transaksi
{
    @Id
    private Long id;

    @Column(columnDefinition = "TIMESTAMP")
    private Date tanggal;

    @ManyToOne
    private Member customer;

    @OneToMany
    private List<TransaksiDetil> lines;
}
