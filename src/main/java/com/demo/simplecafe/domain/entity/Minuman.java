/*
 * Minuman.java
 *
 * Created on May 07, 2021, 3:05 PM
 */
package com.demo.simplecafe.domain.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Irfin A
 */
@Data
@Entity
@Table(name="minuman")
public class Minuman
{
    @Id
    private Long id;

    @Column
    private String nama;

    @Column
    private double harga;
}
