/*
 * MemberDtoAssembler.java
 *
 * Created on May 07, 2021, 4:16 PM
 */
package com.demo.simplecafe.http.controller.assembler;

import com.demo.simplecafe.domain.entity.Member;
import com.demo.simplecafe.http.controller.dto.MemberCreateDto;

/**
 * @author Irfin A
 */
public class MemberDtoAssembler
{
    public Member createEntityFromDto(MemberCreateDto dto)
    {
        Member retval = new Member();
        retval.setNama(dto.nama);
        retval.setLevel(dto.level);

        return retval;
    }
}
