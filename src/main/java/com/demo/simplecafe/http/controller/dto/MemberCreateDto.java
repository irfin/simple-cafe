/*
 * MemberCreateDto.java
 *
 * Created on May 07, 2021, 3:18 PM
 */
package com.demo.simplecafe.http.controller.dto;

/**
 * JSON utk Create data member
 *
 * {
 *     "nama": [nama pelanggan; type=string; mandatory]
 *     "level": [level pelanggan; type=string; mandatory; values={"BRONZE", "GOLD"}]
 * }
 *
 * @author Irfin A
 */
public class MemberCreateDto
{
    public String nama;
    public String level;
}
