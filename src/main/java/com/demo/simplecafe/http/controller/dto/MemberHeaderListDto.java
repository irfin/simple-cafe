/*
 * MemberHeaderListDto.java
 *
 * Created on May 07, 2021, 3:21 PM
 */
package com.demo.simplecafe.http.controller.dto;

import java.util.List;

/**
 * Dokumen JSON untuk daftar pelanggan
 *
 * HTTP STATUS 200, OK
 * {
 *      success: True,
 *      code: 200,
 *      message: "Success getAll"
 *      data:
 *          [
 *              {
 *                  "id": [id pelanggan; long]
 *                  "nama": [nama pelanggan ; string],
 *                  "level": [level pelanggan ; string]
 *              },
 *              {
 *                  ... dst
 *              }
 *          ]
 * }
 *
 * @author Irfin A
 */
public class MemberHeaderListDto
{
    public List<MemberData> data;

    public static class MemberData
    {
        public long id;
        public String nama;
        public String level;
    }
}
