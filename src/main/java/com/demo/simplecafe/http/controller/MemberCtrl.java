/*
 * MemberCtrl.java
 *
 * Created on May 07, 2021, 3:59 PM
 */
package com.demo.simplecafe.http.controller;

import com.demo.simplecafe.domain.entity.Member;
import com.demo.simplecafe.http.controller.assembler.MemberDtoAssembler;
import com.demo.simplecafe.http.controller.dto.MemberCreateDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Irfin A
 */
@RestController
@RequestMapping("/member")
public class MemberCtrl
{
    @PostMapping("/create")
    public ResponseEntity<String> create(@RequestBody MemberCreateDto dto)
    {
        MemberDtoAssembler assembler = new MemberDtoAssembler();

        // Baca DTO lalu simpan sebagai object Entity
        Member entity = assembler.createEntityFromDto(dto);

        // Lempar entity ke service utk disimpan.

        return ResponseEntity.ok("Sukses tambah data member.");
    }
}
